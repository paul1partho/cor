﻿using CORPRAC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace CORPRAC.Controllers
{
    public class UserController : ApiController
    {
        // GET: User
        public UserController()
        {

        }
        coresampleEntities1 db = new coresampleEntities1();

        //public ActionResult Index()
        //{
        //    return View("Index", "_myLayoutPage");
        //}


        public List<User> Get()
        {
            var users = db.Users.ToList();
            return users;

        }
        public List<User> Get(int id)
        {
            var users = db.Users.Where(u => u.UserId == id).ToList();

            return users;
        }
        public IHttpActionResult Put(User user)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            using (var ctx = new coresampleEntities1())
            {
                var existingUser = ctx.Users.Where(s => s.UserId == user.UserId).FirstOrDefault();

                if (existingUser != null)
                {
                    existingUser.Username = user.Username;
                    existingUser.DateOfBirth = user.DateOfBirth;
                    existingUser.Lastname = user.Lastname;
                    existingUser.Phone = user.Phone;
                    existingUser.Email = user.Email;
                    existingUser.Firstname = user.Firstname;
                    ctx.SaveChanges();
                }
                else
                {
                    return NotFound();
                }
            }

            return Ok();
        }
        public IHttpActionResult Post(User users)
        {
            //var result = db.Users.Where(oct => oct.UserId == users.UserId).ToList().SingleOrDefault();
            //return Request.CreateResponse<User>(HttpStatusCode.OK, result);
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            using (var ctx = new coresampleEntities1())
            {
                ctx.Users.Add(new User()
                {
                    Username = users.Username,
                    DateOfBirth = users.DateOfBirth,
                    Lastname = users.Lastname,
                    Email=users.Email,
                    Password=users.Password,
                    Phone=users.Phone,
                    Mobile=users.Mobile
                });

                ctx.SaveChanges();
            }

            return Ok();
        }
        // DELETE: api/User/5
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid User id");

            using (var ctx = new coresampleEntities1())
            {
                var user = ctx.Users
                    .Where(s => s.UserId == id)
                    .FirstOrDefault();

                ctx.Entry(user).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();
            }

            return Ok();
        }
    }
}